window.onload = function() {
    $.urlParam = function(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results) {
            return results[1] || 0;

        } else {
            return 0
        }
    }
    var hash = $.urlParam('register_token')
    var shift_id = $.urlParam('shift_id')
    var work_date = $.urlParam('work_date')
    console.log('hash', hash, 'shift_id', shift_id, 'work_date', work_date)
    if ((hash == 0) || (shift_id == 0) || (work_date == 0)) {

        $('#c_p_error').css({
            "display": "block"
        });
        $('#thankMessage').css({
            "display": "none"
        });
    } else {
        var register_hash = $.urlParam('register_token')
        console.log(register_hash)
        $('#c_p_error').css({
            "display": "none"
        });
        var time = " 08:00 ~ 12:00"
        if (shift_id == 1) {
            time = " 08:00 ~ 12:00"
        } else if (shift_id == 2) {
            time = " 12:00 ~ 16:00"
        } else if (shift_id == 3) {
            time = " 16:00 ~ 20:00"
        } else if (shift_id == 4) {
            time = " 20:00 ~ 24:00"
        }
        var di = localStorage.getItem('d')
        var fi = localStorage.getItem('f')
        var displayNumber = getDisplayNUmber(di, fi)
        console.log(displayNumber)
        $('#dynamic_url').append('<a href="' + FRONT_URL + 'cancel.html?' + 'register_token=' + register_hash + ' ">キャンセルはこちら </a>')
        $('#phone_number').append('<a href="tel:' + displayNumber + '">' + displayNumber + '</a>')
        $('#work_date_value').text('【' + moment(work_date).format('YYYY年MM月DD日') + time + '】')
        $('#thirtySixdate').text('【' + moment(work_date).add('-36', 'hours').format('YYYY年MM月DD日 hh:mm:ss ') + '】')
        $('#thankMessage').css({
            "display": "block"
        });
        //<a href="tel:05058304014">050-5830-4014</a>
    }
}