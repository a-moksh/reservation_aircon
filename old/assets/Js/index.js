/**
 * during first Load the page
 */
// local usrl
// const BASE_URL = "http://api.crm.pcrm.local/v1/"
// const FRONT_URL = 'http://127.0.0.1:5500/'
//     // server url
//     // const BASE_URL = "https://crmapi.pcrm.work/v1/"
//153.127.7.113
window.onload = function() {

    var current = new Date();
    var year = current.getFullYear();
    var month = current.getMonth() + 1;
    var wrapper = document.getElementById("sys_calendar");
    add_calendar(wrapper, year, month);
    $('#t3_available').hide()
        // $('#phoneError').hide()
        // $('input[name="time"]:checked').val('1');
    $('#t2_available').hide()
    $('#t1_available').hide()
    $('#zip_code').val('')
    $('#prefecture').val('')
    $('#adddress').val('')
    $('#family_name').val('')
    $('#phone_number').val('')
    $('#email_address').val('')
    $('#trouble').val('')

    // phone_number

    $('#t4_available').hide()
    $('#onselect_title').text('正しく情報を入力してください')

    $.urlParam = function(name) {
        var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
        if (results) {
            return results[1] || 0;

        } else {
            return 0
        }
    }
    var d = $.urlParam('d')
    var f = $.urlParam('f')
    localStorage.setItem('d', d);
    localStorage.setItem('f', f);
    var di = localStorage.getItem('d')
    var fi = localStorage.getItem('f')
        /*
         <!-- <a class="call" href="tel:0120905855">
                          <span>0120-905-855</span
                  >上記作業は数時間お時間いただく場合があります。
                  <br />ご希望の方はお電話で個別にご予約承ります。
                </a> -->
        */
    var displayNumber = getDisplayNUmber(di, fi)
    $('#display_Number').append('<a class="call" href="tel:' + displayNumber + '">' +
        '<span>' + displayNumber + '</span>' +
        '上記作業は数時間お時間いただく場合があります。<br />' +
        'ご希望の方はお電話で個別にご予約承ります。</a>')
};

function ShiftSelect(value) {
    // if()
    var zip_code = $('#zip_code').val();
    var dayValue = $('#dayValue').val()
    if (!zip_code && !dayValue) {
        alert('郵便番号,住所を入力して希望日付を選択してください')
        $('input[name="time"]').prop('checked', false);
        return false
    } else if (!dayValue && zip_code) {
        alert('希望日付を選択してください')
        $('input[name="time"]').prop('checked', false);
        return false
    } else if (!zip_code && dayValue) {
        alert('郵便番号,住所を入力してください')
        $('input[name="time"]').prop('checked', false);
        return false
    } else {
        var date = moment(dayValue).format('Y-M-D')
        var time = '08:00 ~ 12:00'
        if (value == 1) {
            date = date + ' ' + '08:01:00'
            time = '08:00 ~ 12:00'
        }
        if (value == 2) {
            date = date + ' ' + '12:01:00'
            time = '08:12 ~ 16:00'
        }
        if (value == 3) {
            date = date + ' ' + '16:01:00'
            time = '16:00 ~ 20:00'
        }
        if (value == 4) {
            date = date + ' ' + '20:01:00'
            time = '20:00 ~ 24:00'
        }
        $("#work_date_value").val(date);
        $('#onselect_title').text(moment(dayValue).format('YYYY年MM月DD日 ') + time + ' でご予約いたします')
        return true
    }

}

function changeClass(value) {
    if (value == 1) {
        if ($('#zip_code').val()) {
            $("#zip_enter").addClass("entered");
        } else if (!$('#zip_code').val()) {
            $("#zip_enter").removeClass("entered");
        }
    } else if (value == 2) {
        if ($('#family_name').val()) {
            $("#name_enter").addClass("entered");
        } else if (!$('#family_name').val()) {
            $("#name_enter").removeClass("entered");
        }
    } else if (value == 3) {
        if ($('#phone_number').val()) {
            $("#phone_enter").addClass("entered");
        } else if (!$('#phone_number').val()) {
            $("#phone_enter").removeClass("entered");
        }
    } else if (value == 4) {
        if ($('#email_address').val()) {
            $("#email_enter").addClass("entered");
        } else if (!$('#email_address').val()) {
            $("#email_enter").removeClass("entered");
        }
    } else if (value == 5) {
        if ($('#trouble').val()) {
            $("#trouble_enter").addClass("entered");
        } else if (!$('#trouble').val()) {
            $("#trouble_enter").removeClass("entered");
        }
    }
}
/**
 * create a calender
 * @param {*} wrapper
 * @param {*} year
 * @param {*} month
 */
function add_calendar(wrapper, year, month) {
    wrapper.textContent = null;

    var headData = generate_calendar_header(wrapper, year, month);

    var bodyData = generate_month_calendar(year, month);
    wrapper.appendChild(headData);
    wrapper.appendChild(bodyData);
}
/**
 *
 * @param {*} obj
 */
function click(obj) {}

/**
 *
 * @param {*} year
 * @param {*} month
 * @param {*} day
 */
function addNumber(year, month, day) {
    var daycode = yyyymmdd(year, month, day);
    if ($("#add" + daycode)[0]) return 0;
    $("#sys_sugeehiduke").empty();
    var sss =
        "<span id='add" +
        daycode +
        "' class='sys_sugeehidukes'>" +
        year +
        "年" +
        month +
        "月" +
        day +
        "日<input type='hidden' class='sys_daycode' value='" +
        daycode +
        "'>" +
        "this" +
        "<span onclick='MyRemove(this.parentNode);' class='sugeehidukes_batu'>×</span>　</span>";
    $("#sys_sugeehiduke").append(sss);
}

/**
 * cahnge date
 * @param {*} y
 * @param {*} m
 * @param {*} d
 */
function yyyymmdd(y, m, d) {
    var y0 = ("0000" + y).slice(-4);
    var m0 = ("00" + m).slice(-2);
    var d0 = ("00" + d).slice(-2);
    return y0 + m0 + d0;
}

/**
 * calender header
 * @param {*} wrapper
 * @param {*} year
 * @param {*} month
 */
function generate_calendar_header(wrapper, year, month) {
    var nextMonth = new Date(year, month - 1);
    nextMonth.setMonth(nextMonth.getMonth() + 1);
    var prevMonth = new Date(year, month - 1);
    prevMonth.setMonth(prevMonth.getMonth() - 1);

    var cHeader = document.createElement("nav");
    cHeader.className = "monthSelect nav";

    var cPrev = document.createElement("button");
    cPrev.className = "back";
    cPrev.innerHTML = "前の月";
    cPrev.addEventListener(
        "click",
        function() {
            add_calendar(wrapper, prevMonth.getFullYear(), prevMonth.getMonth() + 1);
            var preStart = moment(prevMonth).startOf('month').format('YYYY-MM-DD') + ' 00:00:00'
            var preEnd = moment(prevMonth).endOf('month').format('YYYY-MM-DD') + ' 23:59:59'

            $('#start_date_value').val(preStart)
            $('#end_date_value').val(preEnd)

            if ($('#zip_code').val()) {
                // var preStart = moment(prevMonth).startOf('month').format('YYYY-MM-DD') + ' 00:00:00'
                // var preEnd = moment(prevMonth).endOf('month').format('YYYY-MM-DD') + ' 23:59:59'
                getAddress(preStart, preEnd)
            }
        },
        false
    );
    cHeader.appendChild(cPrev);

    var cTitle = document.createElement("span");
    cTitle.className = "monthbtn_tuki";
    var cTitleText = document.createTextNode(year + "年" + month + "月");
    cTitle.appendChild(cTitleText);
    cHeader.appendChild(cTitle);

    var cNext = document.createElement("button");
    cNext.className = "next";
    cNext.innerHTML = "次の月";
    cNext.addEventListener(
        "click",
        function() {
            add_calendar(wrapper, nextMonth.getFullYear(), nextMonth.getMonth() + 1);
            var nextStart = moment(nextMonth).startOf('month').format('YYYY-MM-DD') + ' 00:00:00'
            var nextEnd = moment(nextMonth).endOf('month').format('YYYY-MM-DD') + ' 23:59:59'
            $('#start_date_value').val(nextStart)
            $('#end_date_value').val(nextEnd)
            if ($('#zip_code').val()) {
                // var nextStart = moment(nextMonth).startOf('month').format('YYYY-MM-DD') + ' 00:00:00'
                // var nextEnd = moment(nextMonth).endOf('month').format('YYYY-MM-DD') + ' 00:00:00'
                // console.log(nextStart, nextEnd)
                getAddress(nextStart, nextEnd)
                    //var abc = moment(nextMonth).startOf('month').format('YYYY-MM-DD') + ' 00:00:00'
                    //console.log('abc', abc)

            }
        },
        false
    );
    cHeader.appendChild(cNext);

    return cHeader;
}

/**
 *
 * @param {number} year
 * @param {number} month
 */
function get_month_calendar(year, month) {
    var firstDate = new Date(year, month - 1, 1);
    var lastDay = new Date(year, firstDate.getMonth() + 1, 0).getDate();
    var weekday = firstDate.getDay();

    var calendarData = [];
    var weekdayCount = weekday;
    for (var i = 0; i < lastDay; i++) {
        calendarData[i] = {
            day: i + 1,
            weekday: weekdayCount,
        };

        if (weekdayCount >= 6) {
            weekdayCount = 0;
        } else {
            weekdayCount++;
        }
    }
    return calendarData;
}

/**
 * create month calender
 * @param {number} year
 * @param {number} month
 */
function generate_month_calendar(year, month) {
    var current = new Date();
    var now_year = current.getFullYear();
    var now_month = current.getMonth() + 1;
    var now_day = current.getDate(); // + 1;
    var today = current.getDate();


    var weekdayData = ["日", "月", "火", "水", "木", "金", "土"];

    var calendarData = get_month_calendar(year, month);
    var i = calendarData[0]["weekday"];

    while (i > 0) {
        i--;
        calendarData.unshift({
            day: "",
            weekday: i,
        });
    }
    var i = calendarData[calendarData.length - 1]["weekday"];

    while (i < 6) {
        i++;
        calendarData.push({
            day: "",
            weekday: i,
        });
    }

    var cSchedule = document.createElement("div");
    cSchedule.className = "schedule";
    var cTable = document.createElement("table");
    cTable.className = "calendar";
    var insertData = "";
    insertData += "<thead>";
    insertData += "<tr id='month'>";
    for (var i = 0; i < weekdayData.length; i++) {
        insertData += "<th>";
        insertData += weekdayData[i];
        insertData += "</th>";
    }
    insertData += "</tr>";
    insertData += "</thead>";
    insertData += "<tbody id='class_id'>";
    for (var i = 0; i < calendarData.length; i++) {

        var day = calendarData[i]["day"];
        if (!day > 1) {
            insertData += "<td></td>";
            continue;
        }
        var dateFormat = year * 10000 + month * 100 + day;
        var dateFormat_now = now_year * 10000 + now_month * 100 + now_day;

        if (calendarData[i]["weekday"] <= 0) {
            insertData += "<tr id='month_1'>";
        }
        var DayOfweek = calendarData[i]["weekday"];
        if (dateFormat < dateFormat_now) {
            insertData += " <td class=" + "'calendar_close'" + todayflg + " >" + calendarData[i]["day"] + "</td>";
        } else {
            var c = "";
            if (Array.isArray(scdata[dateFormat])) {
                c = scdata[dateFormat].length;
            }
            var c_class = "";
            if (c > 0) {
                c_class = "calendar_work";
            }
            var todayflg = "";
            if (now_year == year && now_month == month && now_day == day) {
                todayflg = "calendar_today";

            }
            var aoihi = "";
            if (calendarData[i]["weekday"] == 6) {
                aoihi = "calendar_aoihi";
            }
            var akaihi = "";
            if (calendarData[i]["weekday"] == 0) {
                akaihi = "calendar_akaihi";
            }
            if (SHUKUZITU[dateFormat]) {
                akaihi = "calendar_akaihi";
            }
            insertData +=
                "<td id=" + dateFormat +
                " class='" +
                todayflg +
                " " +
                aoihi +
                " " +
                akaihi +
                " " +
                c_class +
                "' onclick='SelectDay(" +
                dateFormat +
                "," +
                DayOfweek +
                ");addNumber(" +
                year +
                "," +
                month +
                "," +
                day +
                ")'>";
            insertData += "<span>" + calendarData[i]["day"] + "</span>";
            if (c > 0) insertData += "<span class='sys_sugeecount'>" + c + "</span>";
            insertData += "</td>";
        }

        if (calendarData[i]["weekday"] >= 6) {
            insertData += "</tr>";
        }
    }
    insertData += "</tbody>";

    cTable.innerHTML = insertData;
    cSchedule.innerHTML =
        "<table class='calendar sys_calendar'>" + insertData + "</table>";
    return cSchedule;
}

var scdata = [];
var ybdata = [];

/**
 * when day is selected
 * @param {*} dateinNumber
 * @param {*} DayOfweek
 */
function SelectDay(dateinNumber, day) {
    $("#dayValue").val(dateinNumber);
    var str = $("#zip_code").val();
    if (!str) {
        alert("郵便番号を入力してください");
        return false;
    } else {
        var data = {
            zip_code: str,
            date: dateinNumber,
        };
        $('#sys_calendar').find("*").removeClass("calendar_circle");
        $("#" + dateinNumber + "").addClass('calendar_circle')
        var queryString = Object.keys(data)
            .map((key) => key + "=" + data[key])
            .join("&");
        $.ajax({
            type: "GET",
            url: BASE_URL + "reservation/get-capacities?" + queryString,

            headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            dataType: "json",
            success: function(response, textStatus, xhr) {
                if (xhr.status == 200) {
                    var capacities = response.data.capacities
                    if (capacities.length > 0) {
                        $('#t1l1').addClass('calendar_close')
                        $('#t2l2').addClass('calendar_close')
                        $('#t3l3').addClass('calendar_close')
                        $('#t4l4').addClass('calendar_close')
                        $('#t3').prop("disabled", true)
                        $('#t2').prop("disabled", true)
                        $('#t1').prop("disabled", true)
                        $('#t4').prop("disabled", true)
                        _.forEach(capacities, function(value, key) {
                            if (value.shift_id == 1) {
                                $('#t1_available').show()
                                $('#t1_available').text(value.remaining_capacity + ' 件開いています')
                                if (value.is_close == 0) {
                                    $('#t1l1').addClass('calendar_close')
                                    $('#t1').prop("disabled", true)

                                } else {
                                    $('#t1l1').removeClass('calendar_close')
                                    $('#t1').prop("disabled", false)
                                }
                            }
                            if (value.shift_id == 2) {
                                $('#t2_available').show()
                                $('#t2_available').text(value.remaining_capacity + ' 件開いています')
                                if (value.is_close == 0) {
                                    $('#t2l2').addClass('calendar_close')
                                    $('#t2').prop("disabled", true)

                                } else {
                                    $('#t2l2').removeClass('calendar_close')
                                    $('#t2').prop("disabled", false)
                                }
                            }
                            if (value.shift_id == 3) {
                                $('#t3_available').show()
                                $('#t3_available').text(value.remaining_capacity + ' 件開いています')
                                if (value.is_close == 0) {
                                    $('#t3l3').addClass('calendar_close')
                                    $('#t3').prop("disabled", true)

                                } else {
                                    $('#t3l3').removeClass('calendar_close')
                                    $('#t3').prop("disabled", false)
                                }
                            }
                            if (value.shift_id == 4) {
                                $('#t4_available').show()
                                $('#t4_available').text(value.remaining_capacity + ' 件開いています')
                                if (value.is_close == 0) {
                                    $('#t4l4').addClass('calendar_close')
                                    $('#t4').prop("disabled", true)

                                } else {
                                    $('#t4l4').removeClass('calendar_close')
                                    $('#t4').prop("disabled", false)
                                }
                            }
                        });

                    } else {
                        $('#t1_available').hide()
                        $('#t2_available').hide()
                        $('#t3_available').hide()
                        $('#t4_available').hide()
                        $('#t1l1').addClass('calendar_close')
                        $('#t2l2').addClass('calendar_close')
                        $('#t3l3').addClass('calendar_close')
                        $('#t4l4').addClass('calendar_close')

                    }

                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // console.log(textStatus);
                alert("It is error!!");
            },
        });
        return false;
    }
}
/**
 * call the address when user enter address
 */
function getAddress(monthStart, monthEnd) {
    var firstday = ''
    var defer = [];
    var lastday = ''
        // var start_frominput = ;
    var start_frominput = $('#start_date_value').val()
    var end_frominput = $('#end_date_value').val()
        // console.log('input', start_frominput, end_frominput)
    if (monthStart) {
        firstday = monthStart
    } else if (start_frominput) {
        firstday = start_frominput

    } else {
        firstday = moment().startOf('month').format('YYYY-MM-DD') + ' 00:00:00';
    }
    if (monthEnd) {
        lastDay = monthEnd
    } else if (end_frominput) {
        lastDay = end_frominput
    } else {
        lastDay = moment().endOf('month').format('YYYY-MM-DD') + " 23:59:59";

    }
    var str = $("#zip_code").val();
    if (str) {
        $('#class_id').find("td").addClass("calendar_close");
        var data = {
            zip_code: str,
            start: firstday,
            end: lastDay
        };
        var queryString = Object.keys(data)
            .map((key) => key + "=" + data[key])
            .join("&");
        $.ajax({
            type: "GET",
            url: BASE_URL + "reservation/from-zip?" + queryString,

            headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            dataType: "json",
            success: function(response, textStatus, xhr) {
                if (xhr.status == 200) {
                    var address = response.data.address[0];
                    $("#prefecture").val(address.prefecture_ja);
                    $("#adddress").val(address.address_ja + address.street_address_ja);
                    $('#p_address').addClass('entered')
                    $('#a_address').addClass('entered')
                    var capacities = response.data.capacity
                    var plus_two_days = response.data.plus_two_days
                    if (capacities.length > 0) {
                        _.forEach(capacities, function(value, key) {
                            if (value.shift_day_code >= plus_two_days) {
                                if (value.remaining_capacity > 0) {
                                    $('#' + value.shift_day_code).removeClass('calendar_close')
                                }

                            }
                        })
                    }
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                alert("正しい郵便番号を入力してください");
            },
        });
        return false;
    } else {
        alert("郵便番号入力してください")
    }
}

var waitSugeeScheduleDelete = 0;

function ScheduleShokika(sid) {
    SugeeScheduleLoad(sid);
}


function registerAnken() {
    // window.location.href = FRONT_URL + 'thanks.html';

    var shift_id = $('input[name="time"]:checked').val();
    var familyname = $('#family_name').val()
    var prefecture = $('#prefecture').val()
    var address = $('#adddress').val()
    var phone1 = $('#phone_number').val();
    // var phone2 = $('#email_address').val();
    var trouble = $('#trouble').val();
    var work_date = $('#work_date_value').val()
    var incoming_phone = $('#phone_number').val();
    // var dial_id = ''
    var mail_address = null
        // var mail_address = $('#email_address').val();
    if ($('#email_address').val()) {
        var mail_address = $('#email_address').val();

    }
    var sub_category_id = $('input[name="service"]:checked').val();
    var email = 0
    if (mail_address != null) {
        email = valiateEmail(mail_address)

    }
    var did = 27;
    var di = localStorage.getItem('d')

    if (di != 0) {
        did = di;
    } else {
        did = 27
    }
    var form = {
        shift_id: shift_id,
        familyname: familyname,
        prefecture: prefecture,
        address: address,
        phone1: phone1,

        trouble: trouble,
        work_date: work_date,
        incoming_phone: incoming_phone,
        dial_id: did,
        mail_address: mail_address,
        sub_category_id: sub_category_id,
        email: email,
        field_id: 5
    }

    var result = validateInput(form)
    var phoneVal = phoneValidation();
    console.log(email);
    if (result != false && phoneVal != false) {
        $('#submitButton').prop('disabled', true);
        var trouble_name = ($("#" + sub_category_id).text());
        form.trouble_name = trouble_name
        var queryString = Object.keys(form)
            .map((key) => key + "=" + form[key])
            .join("&");
        $.ajax({
            type: "POST",
            url: BASE_URL + "reservation/create-anken?" + queryString,

            headers: {
                "Content-Type": "application/json",
                "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
            },
            dataType: "json",
            success: function(response, textStatus, xhr) {
                // console.log('res', xhr)
                if (xhr.status == 201) {
                    console.log('res', response.data.hash)
                    var hash = response.data.hash
                    var work_date = moment(response.data.work_date).format('YYYY-MM-DD')
                    response.data.work_date
                    var shift_id = response.data.shift_id
                    var url = FRONT_URL + 'thanks.html?' +
                        'register_token=' + hash +
                        '&shift_id=' + shift_id +
                        '&work_date=' + work_date
                    $('#submitButton').prop('disabled', false);
                    window.location.href = url

                    // window.location.href = FRONT_URL + 'thanks.html?' +
                    //     'register_token=' + hash +
                    //     '&shift_id=' + shift_id +
                    //     '&work_date=' + work_date;
                    // var address = response.data[0];
                    // $("#prefecture").val(address.prefecture_ja);
                    // $("#adddress").val(address.address_ja + address.street_address_ja);
                    // $('#p_address').addClass('entered')
                    // $('#a_address').addClass('entered')
                    // console.log(xhr);
                }
            },
            error: function(XMLHttpRequest, textStatus, errorThrown) {
                // console.log(textStatus);

                alert("登録できません");
            },
        });
        return false;
        // alert('call to api')
    } else {
        alert('入力内容に間違があります')
    }

}

function validateInput(form) {
    if (!form.prefecture) {
        alert('都道府県を入力して下さい')
        return false
    }
    if (!form.familyname) {
        alert("お名前を入力してください")
        return false
    }
    if (!form.work_date) {
        alert("希望時間を入力してください")
        return false
    }
    if (!form.incoming_phone) {
        alert("電話番号入力してください")
        return false
    }
    if (!form.trouble) {
        alert("申し込み内容を入力してください")
        return false
    }
    if (!form.address) {
        alert("住所を入力してください")
        return false

    }
}

function initialLoad(monthStart, monthEnd) {
    var firstday = ''
    var defer = [];
    var lastday = ''
    if (monthStart) {
        firstday = monthStart
    } else {
        firstday = moment().startOf('month').format('YYYY-MM-DD') + ' 00:00:00';
    }
    if (monthEnd) {
        lastDay = lastDay
    } else {
        lastDay = moment().endOf('month').format('YYYY-MM-DD') + " 23:59:59";
    }
    var data = {
        start: firstday,
        end: lastDay
    }
    var queryString = Object.keys(data)
        .map((key) => key + "=" + data[key])
        .join("&");
    $.ajax({
        type: "GET",
        async: false,
        url: BASE_URL + "reservation/initial-load?" + queryString,

        headers: {
            "Content-Type": "application/json",
            "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
        },
        dataType: "json",
        success: function(response, textStatus, xhr) {
            if (xhr.status == 200) {
                defer.push(response);
                // testData = _.cloneDeep(response.data); // return response.data
                // var address = response.data[0];
                // $("#prefecture").val(address.prefecture_ja);
                // $("#adddress").val(address.address_ja + address.street_address_ja);
                // $('#p_address').addClass('entered')
                // $('#a_address').addClass('entered')
                // console.log(xhr);
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            // console.log(textStatus);
            alert("It is error!!");
            //defer.reject(err);
        },
    });

    return defer[0] //.promise()

    // return false;
    // console.log('firar', firstday, 'last', lastDay)
}

function phoneValidation() {
    if ($('#phone_number').val()) {
        var initalPhone = $('#phone_number').val()
        var phoneProcess = initalPhone.replace(/-/g, '');
        var ValidatePhone = Number(phoneProcess)
        if (_.isNaN(ValidatePhone)) {
            // $('#phoneError').show()
            $('#phoneError').css({
                "display": "block"
            });
            return false
        } else {
            $('#phoneError').css({
                "display": "none"
            });
            return true
        }
    } else {
        return false
    }
}

function valiateEmail(address) {
    if (address.match(/[a-zA-Z0-9\._-]+@[a-zA-Z0-9\._-]+/)) {
        return 1;
    } else {
        return 0;

    }
}